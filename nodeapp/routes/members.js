//Declar dependencies and model
const Member = require("../models/members")
const express = require("express")
const router = express.Router() //to handle routing



router.post("/", (req, res) => {
	// console.log("test");
	// return res.send(req.body);
	const member = new Member(req.body);
	//save to database
	member.save()
		.then(() => {
			res.send(member)
		})
		.catch((e) => {
			//BAD REQUEST (http response status codes)
			res.status(400).send(e)
		})
});


router.get("/", (req, res) => {
	// return res.send("get all teams");
	Member.find()
		.then((members) => {
			return res.status(200).send(members)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});

router.get("/:id", (req, res) => {
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id

	//Mongoose Models Query
	Member.findById(_id)
		.then((member) =>{
			if(!member) {
				return res.status(404).send(e)
			}
			return res.send(member)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});

router.patch("/:id", (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id

	//Mongoose Models Query
	Member.findByIdAndUpdate(_id, req.body, { new:true })
		.then((member) => {
			if(!member) {
				//NOT FOUND
				return res.status(404).send(e)
			}
			return res.send(member)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});


router.delete("/:id", (req, res) => {
	// return res.send("delete a team");
	const _id = req.params.id

	//Mongoose Models Query
	Member.findByIdAndDelete(_id)
		.then((member) => {
			if(!member) {
				return res.send(404).send(e)
			}
			return res.send(member)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
})

module.exports = router