//Declar dependencies and model
const Team = require("../models/teams")
const express = require("express")
const router = express.Router() //to handle routing


//Create Routes/Endpoints
//1.) Create
//a.) teams

router.post("/", (req, res) => {
	// console.log("test");
	// return res.send(req.body);
	const team = new Team(req.body);
	//save to database
	team.save()
		.then(() => {
			res.send(team)
		})
		.catch((e) => {
			//BAD REQUEST (http response status codes)
			res.status(400).send(e)
		})
})

router.get("/", (req, res) => {
	// return res.send("get all teams");
	Team.find()
		.then((teams) => {
			return res.status(200).send(teams)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
})

router.get("/:id", (req, res) => {
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id

	//Mongoose Models Query
	Team.findById(_id)
		.then((team) =>{
			if(!team) {
				return res.status(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
})

router.patch("/:id", (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id

	//Mongoose Models Query
	Team.findByIdAndUpdate(_id, req.body, { new:true })
		.then((team) => {
			if(!team) {
				//NOT FOUND
				return res.status(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
})

router.delete("/:id", (req, res) => {
	// return res.send("delete a team");
	const _id = req.params.id

	//Mongoose Models Query
	Team.findByIdAndDelete(_id)
		.then((team) => {
			if(!team) {
				return res.send(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
})

module.exports = router