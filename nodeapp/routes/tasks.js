//Declar dependencies and model
const Task = require("../models/tasks")
const express = require("express")
const router = express.Router() //to handle routing


router.post("/", (req, res) => {
	const task = new Task(req.body)
	task.save()
		.then(() => {
			res.send(task)
		})
		.catch((e) => {
			res.status(400).send(e)
		})
})

router.get("/", (req, res) => {
	// return res.send("get all teams");
	Task.find()
		.then((tasks) => {
			return res.status(200).send(tasks)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});

router.get("/:id", (req, res) => {
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id

	//Mongoose Models Query
	Task.findById(_id)
		.then((task) =>{
			if(!task) {
				return res.status(404).send(e)
			}
			return res.send(task)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});

router.patch("/:id", (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id

	//Mongoose Models Query
	Task.findByIdAndUpdate(_id, req.body, { new:true })
		.then((task) => {
			if(!task) {
				//NOT FOUND
				return res.status(404).send(e)
			}
			return res.send(task)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});

router.delete("/:id", (req, res) => {
	// return res.send("delete a team");
	const _id = req.params.id

	//Mongoose Models Query
	Task.findByIdAndDelete(_id)
		.then((task) => {
			if(!task) {
				return res.send(404).send(e)
			}
			return res.send(task)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
})

module.exports = router